###################################################################################################################
# 09/26/2022 Tony Sanchez LIGO
# The Goal of this script is to fetch historical data, then determine the frequency that the detector is in
# certain states.
###################################################################################################################
# import math
import nds2
import gpstime
import argparse

# To Do List
# TODO: Add functions to return Duration , count and Min and Max arrays.
# TODO: Add a way for users to get minute trends, s-trends, and Maybe Raw.
# TODO: Create the GUI, with toggle buttons for == <= >= != and buttons and farkles.
# TODO: NDSCOPE Button?
# TODO: LLO servers & Channels?
print("*******************************************************************")
print("statecounter.py version 1.3")
print("This script needs arguments to be ran correctly, if you are not passing arguments then this script will fail. "
      "This script can be ran from another python script and the returned results can then be manipulated internally. "
      "For running this script from the terminal, please see help section by running python3 statecounter.py --help "
      "For running this script from another python script please see test.py found in the git lab page "
       "https://git.ligo.org/anthony.sanchez/statecounter")
print('*******************************************************************')


def main(host, port, gpsstart, gpsstop, value, chan, operator, trend="m-trend"):

    # '''rounding function rounds times -nearest minute Credit 4 this f(x) -> Jim's Hepi trends'''
    def jimround(x, base=60):
        return int(base * round(float(x)/base))

    # Get GPS time now round to closest minute
    # gps_start = jimround(int(gpstime.gpsnow()) - 3600*24*30*2)
    gps_start = jimround(gpsstart)
    # gps_stop = jimround(int(gpstime.gpsnow()) - 3600*2)
    gps_stop = jimround(int(gpsstop))
    # print("GPS Start time:", gpstime.tconvert(gps_start), "GPS End time:", gpstime.tconvert(gps_stop),
    #      "Delta t:", (gps_stop - gps_start)/3600)

    # Query Value will be the state that the User is looking for.
    queryvalue = float(value)
    # print(queryValue)
    # Channels are a list of channels that the users would like to look through for a particular value.
    # mean is the mean value of the channel over the m-trend (minute trend)
    # channels = [channel+'.mean,m-trend']
    if trend == "raw":
        channels = [chan + ' ,' + trend]
        print(channels)
    else:
        channels = [chan + ".mean,"+trend]
        print(channels)
    # fetching data from the LHO servers
    buffers = nds2.fetch(channels, gps_start, gps_stop, gap_handler=nds2.STATIC_HANDLER_POS_INF,
                         allow_data_on_tape=True, hostname=host, port=port)
    # The fetching the data from the Caltech servers.
    # buffers = nds2.fetch(channels=channels, gps_start=gps_start, gps_stop=gps_stop, params=params )

    samplerate = buffers[0].channel.sample_rate  # grabs the sample rate for calculating time.
    eol = buffers[0].data.size  # grabs the size of the data returned by the Fetch.
    buffertime = buffers[0].gps_seconds  # grabs the time of the first entry & is the only true un-calculated time value
    calctime = buffertime
    print(buffers[0].channel)
    print("lines read from servers:", eol)

    # Query Value time is the time that the value shows up in the channel.
    queryvaluetime = 1
    querylist = []      # The empty list of times that the channel entered query state.

    if operator == "==":
        for element in range(0, eol):
            # print("Line:", element, "Data value:", buffers[0].data.item(element), "Time:", int(calctime))
            calctime = buffertime + element*(1/samplerate)
            # print (calctime)
            if buffers[0].data.item(element) == queryvalue:
                if queryvaluetime != int(calctime):
                    querylist.append(int(calctime))
                    queryvaluetime = int(calctime)

    elif operator == ">":
        for element in range(0, eol):
            # print("Line:", element, "Data value:", buffers[0].data.item(element), "Time:", int(calctime))
            calctime = buffertime + element*(1/samplerate)
            # print (calctime)
            if buffers[0].data.item(element) > queryvalue:
                if queryvaluetime != int(calctime):
                    querylist.append(int(calctime))
                    queryvaluetime = int(calctime)

    elif operator == ">=":
        for element in range(0, eol):
            # print("Line:", element, "Data value:", buffers[0].data.item(element), "Time:", int(calctime))
            calctime = buffertime + element*(1/samplerate)
            # print (calctime)
            if buffers[0].data.item(element) >= queryvalue:
                if queryvaluetime != int(calctime):
                    querylist.append(int(calctime))
                    queryvaluetime = int(calctime)

    elif operator == "<":
        for element in range(0, eol):
            # print("Line:", element, "Data value:", buffers[0].data.item(element), "Time:", int(calctime))
            calctime = buffertime + element*(1/samplerate)
            # print (calctime)
            if buffers[0].data.item(element) == queryvalue:
                if queryvaluetime != int(calctime):
                    querylist.append(int(calctime))
                    queryvaluetime = int(calctime)

    elif operator == "<=":
        for element in range(0, eol):
            # print("Line:", element, "Data value:", buffers[0].data.item(element), "Time:", int(calctime))
            calctime = buffertime + element*(1/samplerate)
            # print (calctime)
            if buffers[0].data.item(element) == queryvalue:
                if queryvaluetime != int(calctime):
                    querylist.append(int(calctime))
                    queryvaluetime = int(calctime)

    print("Number of time values where Query state", chan, operator, "state", value, "is true:", len(querylist))
    statecounterstart = []
    statecounterstop = []
    duration = []
    # prints Duration of each period of time where the query state is true for minute trends, this should be a function.
    if len(querylist) > 0:
        # print("Duration of Query States:")
        for entry in range(0, len(querylist)-1):  # loop that loops through the list even if it's odd numbered.
            if entry == 0:
                statecounterstart.append(querylist[entry])
            if (querylist[entry+1]-querylist[entry]) != 60:  # checks if the time between entries is not 60 sec
                statecounterstop.append(querylist[entry])  # appends query time-> queryStateCounter defines end of entry
                duration.append(statecounterstop[-1]-statecounterstart[-1])
                # print((statecounterstop[-1] - statecounterstart[-1])/3600)
            if entry != 0 and (querylist[entry-1]-querylist[entry]) != -60:
                # print("start of new state", (querylist[entry-1]-querylist[entry]))
                statecounterstart.append(querylist[entry])
            if entry == (len(querylist)-2):
                statecounterstop.append(querylist[-1])
                duration.append(statecounterstop[-1] - statecounterstart[-1])
                # print((statecounterstop[-1] - statecounterstart[-1])/3600)

        print("Number of Unique times the channel was in such a state:", len(statecounterstart))  # len(statecounterstop
    # End of function
    print("Length of SCstart:", len(statecounterstart), "Length of SCstop:", len(statecounterstop),
          "length of duration", len(duration))
    if len(querylist) > 0:
        for n in range(0, len(statecounterstart)-1):
            print(statecounterstart[n], statecounterstop[n], (statecounterstop[n] - statecounterstart[n]), duration[n])
            if n == len(statecounterstart)-2:
                print(statecounterstart[-1], statecounterstop[-1], (statecounterstop[-1]-statecounterstart[-1]))

    print("Counting states completed")
    return statecounterstart, statecounterstop, duration


if __name__ == '__main__':

    # Defining Flags & Parsing.
    parser = argparse.ArgumentParser(description='command line flag parser')
    parser.add_argument('-chan', '--channel', default="H1:GRD-ISC_LOCK_STATE_N",
                        help='Default is "H1:GRD-ISC_LOCK_STATE_N"')
    parser.add_argument('-operator', '--operator', '--OPERATOR',
                        help='Inequalities operator: example: -op ">" ,-op "<", -op ">=", -op "<=" , or -op "=="')
    parser.add_argument('-value', '--value',
                        help='The value/thresh-hold that you are looking for in a channel. Default is "600" ')
    parser.add_argument("-host", "--hostname", default="h1daqnds1",
                        help='Server name default: -host "c"')
    parser.add_argument("-port", "--portnumber", default="8088", help="default port is 8088")
    parser.add_argument("-trend", "--trend", default="m-trend",
                        help='Default: minute trend ("m-trend"), s-trend, raw coming soon')
    parser.add_argument("-gpsstart", "--gpsstart", default="1368946818",
                        help='Default is Start of O4 Run, example: -gpsstart "1368946818" ')
    parser.add_argument("-gpsstop", "--gpsstop", default=gpstime.gpsnow(),
                        help='Default gpsstop grabs current GPS time. Example: -gpsstop "1358788522"')
    args = parser.parse_args()

    chan = args.channel
    operator = args.operator
    value = args.value
    host = args.hostname
    port = args.portnumber
    gpsstart = args.gpsstart
    gpsstop = args.gpsstop
    trend = args.trend

    print("channel {} " .format(args.channel))
    print("operator {} " .format(args.operator))
    print("value {} " .format(args.value))
    print("host {} " .format(args.hostname))
    print("port {} ".format(args.portnumber))
    print("trend {} " .format(args.trend))
    print("gpsstart {} " .format(args.gpsstart))
    print("gpsstop {} " .format(args.gpsstop))

    main(host, port, gpsstart, gpsstop, value, chan, operator, trend="m-trend")
