import statecounter
import matplotlib.pyplot as plt

# gpstime from the start of the run = 1368946818
# gpstime from start of O4B: gpsstart="1395964818"
# statecounter.main(host='h1daqnds1', gpsstart=1368946818, gpsstop=gpstime.gpsnow(), value="69", operator="==")
# '''
# Todo add minimum and maximum to the list of returned values
scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1397779218",
                                              gpsstop="1398788764", value=101, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")

print("Times ISC_Lock was in Aquire_DRMI_1F [101]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1

    # print(i, scstart[-i], scstop[-i], duration[-i], min(duration), minimum, max(duration), maximum)
print("----------------------------------------")

plt.hist(duration)
plt.xlabel("Duration of State")
plt.ylabel(" # Of times State was observed @ Duration ")
plt.show()
