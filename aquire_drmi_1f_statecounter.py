import statecounter
import time
import gpstime
# gpstime from the start of the run = 1368946818
# statecounter.main(host='h1daqnds1', gpsstart=1368946818, gpsstop=gpstime.gpsnow(), value="69", operator="==")

# Todo add minimum and maximum to the list of returned values
scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=101, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")


print("Times ISC_Lock was in Aquire_DRMI_1F [101]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
# print(i, scstart[-i], scstop[-i], duration[-i], min(duration), minimum, max(duration), maximum)
print("----------------------------------------")
print("")

scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=102, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")

print("Times ISC_Lock was in DRMI_LOCKED_PREP_ASC [102]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
print("----------------------------------------")
print("")

scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=103, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")

print("Times ISC_Lock was in ENGAGE_DRMI_ASC [103]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
print("----------------------------------------")
print("")

scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=104, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")


print("Times ISC_Lock was in TURN_ON_BS_STAGE2 [104]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
print("----------------------------------------")
print("")

scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=105, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")


print("Times ISC_Lock was in TRANSITION_DRMI_TO_3F [105]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
print("----------------------------------------")
print("")

scstart, scstop, duration = statecounter.main(host="h1daqnds0", port="8088", gpsstart="1395964818",
                                              gpsstop="1398788764", value=110, operator="==",
                                              chan="H1:GRD-ISC_LOCK_STATE_N", trend="m-trend")


print("Times ISC_Lock was in DRMI_LOCKED_CHECK_ASC [110]")
print("Start:", "Stop:", "Duration:", "Min:", "Max:")
count = len(scstart)
i = 0
while i < count:
    print(i, scstart[i], scstop[i], duration[i])
    i = i + 1
print("----------------------------------------")
print("")
